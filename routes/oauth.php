<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| OAuth Overrides
|--------------------------------------------------------------------------
|
| Perform any additional functionality before passing over to Laravel Passport
|
*/

Route::post('/oauth/token', function (Request $request) {
    // $userType = $request->input('user_type') ?? 'eCommerce';

    // if ($userType !== 'eCommerce' && $userType !== 'Merchant') {
    //     return Response::json([
    //         'error' => 'unsupported_user_type',
    //         'error_description' => 'The user type is not supported by the authorization server.',
    //         'hint' => 'Valid user types are Merchant and eCommerce',
    //         'message' => 'The user type is not supported by the authorization server.',
    //     ], 400);
    // }

    // // Save the portal_user_type in a temporary variable, this will be used to login to Sugar later in User->findAndValidateForPassport(...)
    // \Config::set('temp_portal_user_type', $userType);

    return \App::call('Laravel\Passport\Http\Controllers\AccessTokenController@issueToken');
});
