FROM nginx:1.21

COPY docker/ngnix/vhost.conf /etc/nginx/conf.d/default.conf

COPY docker/ngnix/certs/webinar-app-api.dev.crt /etc/nginx/certs/webinar-app-api.dev.crt
COPY docker/ngnix/certs/webinar-app-api.dev.key /etc/nginx/certs/webinar-app-api.dev.key

RUN ln -sf /dev/stdout /var/log/nginx/access.log \
	&& ln -sf /dev/stderr /var/log/nginx/error.log