# webinar-app-api


## Installation

1. Set up `.env` file to point to correct database settings.
1. If the database is not created yet, create a local database called `webinar-app-api`
1. Setup .env file to point to correct database settings
1. Build the containers and run using:

    ```
    docker-compose build
    docker-compose up -d
    ```

    This will serve the site at http://webinar-app-api.local 

1. Run the commands:
    ```
    # Preferred method
    docker-compose exec php composer install
    docker-compose exec php php artisan migrate
    docker-compose exec php php artisan db:seed

    # OR, if you have PHP and composer installed on your local machine:
    composer install
    php artisan migrate
    php artisan db:seed
    ```

## Authentication

- A JWT token will be created at the time login/register
- Favourite posts and user info needs the token

## To DO
- Integration with circle/ci to do the deployment


