<?php

namespace App\Http\Controllers;

use App\Models\Posts;
use Illuminate\Http\Request;

class PostsController extends Controller
{
    /**
     * Display a listing of all the posts
     *
     * @return \Illuminate\Http\Response
     */
    public function getAll(int $pageSize=12)
    {
        try {
            $response = Posts::paginate($pageSize);
            return \Response::json($response);
        } catch (\Illuminate\Auth\AuthenticationException $aex) {
            return \Response::json(['status' => 'error', 'message' => $aex->getMessage()], 401);
        } catch (\Exception $ex) {
            $statusCode = method_exists($ex, 'getStatusCode') ? $ex->getStatusCode() : 500;
            return \Response::json(['status' => 'error', 'message' => $ex->getMessage()], $statusCode);
        }
    }
}
