<?php
declare(strict_types=1);

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\DTO\FavouritePostData;
use App\Models\Posts;
use DB;
use Illuminate\Http\JsonResponse;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class FavouritePostsController extends Controller
{
    /**
     * Method to create a fav post in the favourite_post table.
     * @param request $Request
     * @param int $postId
     *
     * @throws GuzzleException
     * @return JsonResponse
     */
    public function create(Request $request, int $postId): JsonResponse
    {
        try {
            $favouritePostsData = FavouritePostData::fromRequest($request, $postId);
            $favouritePost = new \App\Models\FavouritePosts();


            // Check if fav post is available or not
            $post = \App\Models\Posts::find($postId);
            if ($post === null) {
                throw new NotFoundHttpException("Post with id '{$postId}' not found");
            }

            $favouritePost->postId = $favouritePostsData->postId;
            $favouritePost->userId = $favouritePostsData->userId;

            // Check if fav post is already exist for the same user
            if($favouritePost->where('userId','=',$request->user()->id)->where('postId','=',$postId)->first())
            {
                throw new HttpException(409, "Post is already in user's favorite list");
            } 

            $favouritePost->save();
            $response = FavouritePostData::fromModel($favouritePost);
            return \Response::json($response->toJsonResponse());
        }
        catch (\Illuminate\Auth\AuthenticationException $aex) {
            return \Response::json(['status' => 'error', 'message' => $aex->getMessage()], 401);
        } catch (\Exception $ex) {
            $statusCode = method_exists($ex, 'getStatusCode') ? $ex->getStatusCode() : 500;
            return \Response::json(['status' => 'error', 'message' => $ex->getMessage()], $statusCode);
        }

    }

    /**
     * Fetch all the posts favourite based upon the userId
     * @param request $Request
     *
     * @throws GuzzleException
     * @return JsonResponse
     */
    public function getAll(Request $request): JsonResponse
    {
 
        try {

            $response = DB::table('posts')
            ->leftJoin('favourite_posts', 'posts.id', '=', 'favourite_posts.id')
            ->where('favourite_posts.userId', '=', $request->user()->id)
            ->select('posts.title', 'posts.content')
            ->get();
            return \Response::json($response);

        } catch (\Illuminate\Auth\AuthenticationException $aex) {
            return \Response::json(['status' => 'error', 'message' => $aex->getMessage()], 401);
        } catch (\Exception $ex) {
            $statusCode = method_exists($ex, 'getStatusCode') ? $ex->getStatusCode() : 500;
            return \Response::json(['status' => 'error', 'message' => $ex->getMessage()], $statusCode);
        }
    }

    /**
     * Delete the post from the favourite_post (to make it unfavourite)
     * @param Request $request
     * @param int $postId
     *
     * @throws GuzzleException
     * @return JsonResponse
     */
    public function delete(Request $request, int $postId): JsonResponse
    {
        try {
            $favouritePost = new \App\Models\FavouritePosts();

            $response = $favouritePost->where('userId' ,'=', $request->user()->id)->where('postId','=', $postId)->delete();
            return \Response::json($response);

        } catch (\Illuminate\Auth\AuthenticationException $aex) {
            return \Response::json(['status' => 'error', 'message' => $aex->getMessage()], 401);
        } catch (\Exception $ex) {
            $statusCode = method_exists($ex, 'getStatusCode') ? $ex->getStatusCode() : 500;
            return \Response::json(['status' => 'error', 'message' => $ex->getMessage()], $statusCode);
        }
    }


}
