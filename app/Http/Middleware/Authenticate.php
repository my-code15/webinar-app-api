<?php

namespace App\Http\Middleware;

use Illuminate\Auth\Middleware\Authenticate as Middleware;

class Authenticate extends Middleware
{
    /**
     * Get the path the user should be redirected to when they are not authenticated.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return string|null
     */
    protected function redirectTo($request)
    {
        $middlewares = optional($request->route())->middleware() ?? [];
        if (! $request->expectsJson() && in_array('web', $middlewares, true)) {
            return route('login');
        }
    }
}
