<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class FavouritePosts extends Model
{
    use HasFactory;

     /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'userId',
        'postId',
    ];

    public function posts()
    {
        return $this->belongs_to('Posts');
    }
    public function users()
    {
        return $this->belongs_to('User');
    }
}
