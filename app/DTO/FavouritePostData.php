<?php

declare(strict_types=1);

namespace App\DTO;

use Illuminate\Http\Request;
use Spatie\DataTransferObject\DataTransferObject;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use App\Models\FavouritePosts;

class FavouritePostData extends DataTransferObject
{

    /** @var int */
    public $userId;

    /** @var int */
    public $postId;

    /**
     * Initialises a DTO from an Eloquent model.
     *
     * @param \App\Models\Checkouts $checkout
     * @return static
     */
    public static function fromModel(FavouritePosts $favouritePost): self
    {
        return new self([
            'userId' => $favouritePost->userId,
            'postId' => $favouritePost->postId,
        ]);
    }


      /**
     * Returns an array that can be used as a standard JSON response for clients.
     *
     * @return array
     */
    public function toJsonResponse(): array
    {
        return $this->only(
            'id',
            'userId',
            'postId'
        )->toArray();
    }


     /**
     * Initialises a DTO from a HTTP Request.
     *
     * @param Request $request
     * @return static
     */
    public static function fromRequest(Request $request, $postId): self
    { 

        return new self([
            'userId' => $request->user()->id,
            'postId' => (int) $postId,
        ]);
    }


}